var app_path = '/js';

// modules

var modules_path = app_path + '/app/modules';
var booking_modules_path = modules_path + "/bookings";


// routes

var routes_path = app_path + '/app/routes';
var booking_routes_path = routes_path + "/bookings";
