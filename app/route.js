	
(function() {

	function config ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/booking', {
				templateUrl: booking_routes_path + '/daily/views/main.html',
				controller: 'BookingMainCtrl',
				controllerAs: 'main'
			});

		//$locationProvider.html5Mode(true);
	}

	angular
		.module('bookingApp.route', ['ngRoute'])
		.config(config);

})();