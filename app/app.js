(function() {

	//angular.module('app', ['bookingApp']);


	angular.module('bookingApp', ['bookingApp.route',
								  'bookingApp.factory',
								  'bookingApp.controller',
								  'bookingApp.directive.listing',
								  'bookingApp.directive.calendar',
								  'bookingApp.directive.grid']);


})();