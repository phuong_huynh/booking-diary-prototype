(function() {

    function BookingDailyFactory() {

        var BookingDailyFactory = {};

        BookingDailyFactory.selected_date = Date.now();
        BookingDailyFactory.selected = moment(Date.now());
        
        BookingDailyFactory.today = moment(Date.now());

        BookingDailyFactory.dates = {};
        BookingDailyFactory.dates.current_dates = [];
        BookingDailyFactory.dates.previous_dates = [];
        BookingDailyFactory.dates.future_dates = [];

        BookingDailyFactory.updateCalendar = function() {

            if(BookingDailyFactory.dates.current_dates.length > 0) {
                var weekStart = BookingDailyFactory.dates.current_dates[0].format("YYYY-MM-DD");
                var weekEnd = BookingDailyFactory.dates.current_dates[6].format("YYYY-MM-DD");

                if(BookingDailyFactory.selected.isBefore(weekStart) || BookingDailyFactory.selected.isAfter(weekEnd)) {
                    BookingDailyFactory.updateCalendarDates();
                }

            } else {
                BookingDailyFactory.updateCalendarDates();
            }

        }

        BookingDailyFactory.updateCalendarDates = function() {

            for(var i = 0; i < 7; i++) {
                BookingDailyFactory.dates.current_dates[i] = moment(BookingDailyFactory.selected_date).day(i);                       
            }

            for(var i = 0; i < 7; i++) {
                BookingDailyFactory.dates.previous_dates[i] = moment(BookingDailyFactory.selected_date).day(i-7);                        
            }

            for(var i = 0; i < 7; i++) {
                BookingDailyFactory.dates.future_dates[i] = moment(BookingDailyFactory.selected_date).day(i+7);                      
            }
        }

        BookingDailyFactory.updateSelectedDate = function() {
            BookingDailyFactory.selected_date = BookingDailyFactory.selected.format("YYYY-MM-DD");            
            //BookingDailyFactory.updateCalendar();
        }

        BookingDailyFactory.setSelectedDate = function(date) {
            BookingDailyFactory.selected = moment(date);
            BookingDailyFactory.updateSelectedDate();
        }

        BookingDailyFactory.setNextDay = function() {
            BookingDailyFactory.selected.add('d',1);
            BookingDailyFactory.updateSelectedDate();
        }

        BookingDailyFactory.setPreviousDay = function() {
            BookingDailyFactory.selected.subtract('d',1);
            BookingDailyFactory.updateSelectedDate();
        }

        BookingDailyFactory.setNextWeek = function() {
            BookingDailyFactory.selected.add('w',1);
            BookingDailyFactory.updateSelectedDate();
        }

        BookingDailyFactory.setPreviousWeek = function() {
            BookingDailyFactory.selected.subtract('w',1);
            BookingDailyFactory.updateSelectedDate();
        }

        return BookingDailyFactory;
    };

    angular
        .module('bookingApp.factory')
        .factory('BookingDailyFactory', BookingDailyFactory);


})();