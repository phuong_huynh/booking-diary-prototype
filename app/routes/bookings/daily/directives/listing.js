(function() {


	function bookingListingCtrl($scope, BookingDailyFactory) {
		this.selected = BookingDailyFactory.selected;


		this.updateSelectedDay = function() {
			this.selected = BookingDailyFactory.selected;
		};

		this.previousDay = function() {
			this.animate = "animate-slide-right";
			BookingDailyFactory.setPreviousDay();
			BookingDailyFactory.updateCalendar();
			this.updateSelectedDay();
			this.resetAnimate();
		};

		this.nextDay = function() {
			this.animate = "animate-slide-left";
			BookingDailyFactory.setNextDay();
			BookingDailyFactory.updateCalendar();
			this.updateSelectedDay();
			this.resetAnimate();
		};

		this.resetAnimate = function() {
			var self = this;

			setTimeout(function() {
				//self.updateValues();
				//self.updateCalendar();
				self.animate = "";
				self.translate = 0;
				$scope.$apply();

			}, 500);
		}
	}

	function bookingDailyListing($swipe, BookingDailyFactory) {

		return {
			restrict: 'E',
			replace: true,
			link: function(scope, element, attrs, ctrl) {

				var active = false;
				var startX;
						
		        $swipe.bind(element, {
		          'start': function(coords) {
		            startX = coords.x;
		            active = true;
		          },
		          'move': function(coords) {

		          	if(active) {
		            
		            	var delta = coords.x - startX;
		            	
						if(delta > 40) {
							ctrl.previousDay();
							active = false;
							scope.$apply();
						} else if(delta < -40) {
							ctrl.nextDay();
							active = false;
							scope.$apply();
						} else {
							ctrl.translate = delta;
							scope.$apply();
						}
					}
		          },
		          'end': function(coords) {
		          	ctrl.translate = 0;
		          	active = false;
		          	scope.$apply();
		          }
		        });
			},
			templateUrl: booking_routes_path + '/daily/directives/listing.html',
			controllerAs: 'bookingListingCtrl',
			controller: bookingListingCtrl
		};
	}

	angular
        .module('bookingApp.directive.listing', ['angularMoment', 'ngTouch'])
		.directive('bookingDailyListing', ['$swipe', 'BookingDailyFactory', bookingDailyListing]);


})();