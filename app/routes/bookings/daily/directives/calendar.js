(function() {

	// controller 

	function bookingCalendarCtrl($scope, BookingDailyFactory) {

		this.animate = "";
		this.dates = [];
		this.translate = 0;
				
		this.selected = BookingDailyFactory.selected;
		this.today = BookingDailyFactory.today;
		
		this.updateCalendar = function() {					
			BookingDailyFactory.updateCalendar();
			this.dates = BookingDailyFactory.dates;						
		}

		this.updateValues = function() {
			this.selected = BookingDailyFactory.selected;
		}

		this.selectDate = function(date) {
			BookingDailyFactory.setSelectedDate(date);
			this.updateValues();
		}

		this.getDateClass = function(date) {
			var date_class = "";

			if( date.format("YYYY-MM-DD") == this.today.format("YYYY-MM-DD") ) {
				date_class = "today";
			}

			if( date.format("YYYY-MM-DD") == this.selected.format('YYYY-MM-DD') ) {
				date_class += " active";
			}

			return date_class;
		}

		this.getTranslateStyle = function() {
			return {'-webkit-transform': 'translate3d(' + bookingCalendarCtrl.translate + 'px,0,0)'};
		}

		this.nextWeek = function() {
			BookingDailyFactory.setNextWeek();
			this.animate = "animate-slide-left";
			this.resetAnimate();	
		}

		this.prevWeek = function() {
			BookingDailyFactory.setPreviousWeek();	
			this.animate = "animate-slide-right";
			this.resetAnimate();		
		}	

		this.resetAnimate = function() {
			var self = this;

			setTimeout(function() {
				self.updateValues();
				self.updateCalendar();
				self.animate = "";
				self.translate = 0;
				$scope.$apply();

			}, 500);
		}

		this.updateCalendar();

	};

	// link

	

	// directive

	function bookingDailyCalendar($swipe, BookingDailyFactory) {

		return {
			restrict: 'E',
			replace: true,
			link: function(scope, element, attrs, ctrl) {
				
				var active = false;
				var startX;
						
		        $swipe.bind(element, {
		        	'start': function(coords) {
		            	startX = coords.x;
		            	active = true;
		          	},
		          	'move': function(coords) {

			          	if(active) {
			            
			            	var delta = coords.x - startX;
			            	
							if(delta > 30) {
								ctrl.prevWeek();
								active = false;
								scope.$apply();
							} else if(delta < -30) {
								ctrl.nextWeek();
								active = false;
								scope.$apply();
							} else {
								ctrl.translate = delta;
								scope.$apply();
							}
						}
					},
					'end': function(coords) {
						ctrl.translate = 0;
						active = false;
						scope.$apply();
					}

		        });

		        
				
			},
			templateUrl: booking_routes_path + '/daily/directives/calendar.html',
			controllerAs: 'bookingCalendarCtrl',
			controller: bookingCalendarCtrl
		};

	}
	
	angular
        .module('bookingApp.directive.calendar', ['angularMoment','ngTouch'])
		.directive('bookingDailyCalendar', ['$swipe', 'BookingDailyFactory', bookingDailyCalendar]);


})();