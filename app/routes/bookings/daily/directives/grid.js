(function() {

	function bookingDailyGrid() {

		return {
			restrict: 'E',
			replace: true,
			link: function(scope, element, attrs, ctrl) {
				//console.log(attrs.startTime);
				ctrl.startTime = moment(Date.now()).hour(6).minute(0).second(0);
				ctrl.endTime = moment(Date.now()).hour(18).minute(0).second(0);				
				ctrl.step = parseInt(attrs.step);

				ctrl.createHours();

			},
			templateUrl: booking_routes_path + '/daily/directives/grid.html',
			controllerAs: 'bookingGridCtrl',
			controller: function($scope) {
				this.startTime = {};
				this.endTime = {};
				this.step = 0;
				this.rows = [];

				this.createHours = function() {
					var currentHour = moment(this.startTime);
					var counterStart = this.startTime.hours() * 60;
					var counterEnd = (this.endTime.hours() * 60) - counterStart;


					for(var i = 0; i <= counterEnd; i = i + this.step) {

						if((i % 60) == 0 ) {

							this.rows.push(
								{
									time: moment(currentHour).add('minutes',i),
									displayLine: true,
									lineStyle: "solid",
									displayTime: true
								}
								
							);

						} else {

							this.rows.push(
								{
									time: moment(currentHour).add('minutes',i),
									displayLine: true,
									lineStyle: "dotted",
									displayTime: false
								}
							);

						}


					}

					// console.log(this.hours);
					//console.log(moment(currentHour));
					//currentHour.add('hours',1);
					// console.log(moment(currentHour.add('hours',1)));

					// while(currentHour.hours() < this.endTime.hours()) {
					// 	this.hours.push(moment(Date.now()).hour(currentHour).minute(0).second(0));				
					// 	currentHour.hours(+1);
					// }
				}
			}
		};
	}

	angular
        .module('bookingApp.directive.grid', [])
		.directive('bookingDailyGrid', bookingDailyGrid);


})();