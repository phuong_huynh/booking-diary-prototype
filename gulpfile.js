var gulp = require('gulp');
var sync = require('browser-sync');
var sass = require('gulp-sass');
var clean = require('gulp-rimraf');
var mainBowerFiles = require('main-bower-files');
var inject = require("gulp-inject");
var order = require("gulp-order");
var es = require('event-stream');
var connect = require('gulp-connect');
var neat = require('node-neat').includePaths;

var reload = sync.reload;

gulp.task('localconnect', function () {
	connect.server({
		root: "dist",
		port: "3000"
	});
});

gulp.task('localbuild', ['localstyle', 'localapp','localbower','locallibs','localhtml'], function() {
	return 1;
	// gulp.src(["app.appcache"])
	//     .pipe(gulp.dest("dist"));	

	
	
});

gulp.task('localstyle',function() {
	var stream = gulp.src("styles/app.scss")
			   		.pipe(sass({
         			   includePaths: ['styles'].concat(neat)
        			}))
			   		.pipe(gulp.dest("dist/css"));

	return stream;
});

gulp.task('localhtml', ['localapp','localbower'], function() {

	var stream = es.merge(
					gulp.src(["dist/js/vendor/*.js"], {read:true}),
					gulp.src(["dist/js/app/**/*.js"], {read:true})
				)
					.pipe(
						order(
							[
								'**/jquery.js',
								'**/angular.js',
								'**/moment.js',
								'**/vendor/**/*.*',
								'**/app/**/*.*'
							]
						)
					)
				.pipe(inject("index.html", {'ignorePath':'/dist'}))
				.pipe(gulp.dest("dist"))
		        .pipe(reload({stream:true}));


	//var stream = gulp.src("index.html")
					// .pipe(inject(files, {ignorePath: '/dist'}))
					// .pipe(gulp.dest("dist"))
			  //       .pipe(reload({stream:true}));

	return stream;
});

gulp.task('localapp', function() {
	var stream = gulp.src("app/**/*.*")
					.pipe(gulp.dest("dist/js/app/"))
					.pipe(reload({stream:true}));

	return stream;

});

gulp.task('locallibs', function() {
	var stream = gulp.src("lib/**/*.js")
					.pipe(gulp.dest("dist/js/vendor/"));
});

gulp.task('localbower', function() {
	var stream = gulp.src(mainBowerFiles({"env":"development"}))
					.pipe(gulp.dest("dist/js/vendor"));

	return stream;

});

gulp.task('localwatch', function() {

	gulp.watch(["app/**/*.*","index.html","styles/app.scss"], ['localbuild']);
	
});

gulp.task('dev', ['devbuild']);

gulp.task('devbuild', ['devconnect', 'devhtml'], function() {
	return 1;
});

gulp.task('devhtml', ['devapp', 'devbower', 'devstyle'], function() {

	var stream = es.merge(
					gulp.src(["dist/js/vendor/*.js"], {read:true}),
					gulp.src(["dist/js/app/**/*.js"], {read:true})
				)
					.pipe(
						order(
							[
								'**/angular.min.js',
								'**/moment.min.js',
								'**/vendor/**/*.*',
								'**/app/**/*.*'
							]
						)
					)
				.pipe(inject("index.html", {'ignorePath':'/dist'}))
				.pipe(gulp.dest("dist"))
		        .pipe(reload({stream:true}));

    return stream;
});

gulp.task('devstyle', ['clean'], function() {
	var stream = gulp.src("styles/app.scss")
			   		.pipe(sass({
         			   includePaths: ['styles'].concat(neat)
        			}))
			   		.pipe(gulp.dest("dist/css"));

	return stream;
});

gulp.task('devconnect', ['clean'], function() {
	return gulp.src(["start.js","package.json"])
			   .pipe(gulp.dest("dist"));	
});

gulp.task('devapp', ['clean'], function() {
	return gulp.src("app/**/*.*")
		       .pipe(gulp.dest("dist/js/app/"));
});

gulp.task('devbower', ['clean'], function() {
	return gulp.src(mainBowerFiles({"env":"production"}))
		.pipe(gulp.dest("dist/js/vendor"));
})

gulp.task('clean', function() {
	return gulp.src('./dist')
			   .pipe(clean());
});




gulp.task('default', ['localbuild','localwatch','localconnect']);
